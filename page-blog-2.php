<?php get_header(); ?>

<!-- blog -->
<div class="wrapper-blog-home page-blog-bg">
	<div class="row">
      	<div class="col-lg-12">
	      <div class="wrap-page-title">
	        <div class="title-wrapper-blog bg-image-page">
	        	<div class="title-page-section text-center">
	        		<h3><?php the_title(); ?></h3>	
	        	</div>
	        </div>
	      </div>
	    </div>
    </div>
  <div class="container blog-section">
    <div class="content-blog-home">   
        <div class="wrap-page-blog">
          <div class="row">
            <div class="article">
              <div class="post">
              <?php 
                $args = array(
                              'post_type'      => 'post',
                              'category_name'  => 'blog',
                              'posts_per_page' => 9 
                              );
                $custom_query2 = new WP_Query($args);
              ?>
              <?php if( $custom_query2 -> have_posts() ) : ?>
                <?php while ( $custom_query2 -> have_posts() ) : $custom_query2 -> the_post(); ?>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 blog-page-fade">
                  <div class="thumbnail blog-home blog-page-section">
                    <?php if(has_post_thumbnail()) : ?>
                      <?php the_post_thumbnail(); ?>
                        <?php endif; ?>
                      <div class="caption caption-blog-page">
                          <h3 style="margin-top: 15px;"><?php the_title()?></h3>                            
                            <div class="view-more-blog-page">
                            	<a href="<?php the_permalink(); ?>" class="btn btn-default btn-xs btn-view-more" role="button">View More</a>
                            </div>
                      </div>
                  </div>
                </div>
                <?php endwhile; ?>
              <?php endif; ?>
              </div> <!-- end post -->
            </div> <!-- end article -->
          </div> <!-- end row -->
        </div> <!-- end wrap -->		    	
    </div> <!-- end content blog home -->
  </div> <!-- end container -->
</div> <!-- end -->

<?php get_footer(); ?> 