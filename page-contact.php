<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<div class="wrapper-page-contact">
	<div class="container contact-section me-contact">
		<div class="wrapper-content">
			<div class="row">
				<div class="article">
					<div class="title-contact">
						<?php 
                			$args = array(
				                              'post_type'      => 'post',
				                              'category_name'  => 'Contact me 1',
				                              'posts_per_page' => 1
			                              );
                			$contact_me_1 = new WP_Query($args);
              			?>
			            <?php if( $contact_me_1 -> have_posts() ) : ?>
			                <?php while ( $contact_me_1 -> have_posts() ) : $contact_me_1 -> the_post(); ?>
						<div class="col-lg-12">
							<div class="wrapper-title">
								<div class="title-header-contact">
									<h2 class="contact-title-page"><?php the_title(); ?></h2>
									<br>
									<h2 class="contact-title-page"><?php the_content(); ?></h2>
								</div>
							</div>
						</div>
							<?php endwhile; ?>
						<?php endif; ?>          <!-- end loop contact 1 --> 
					</div>
				</div>
			</div>
		</div>
	</div>
				<div class="wrapper-office-address" style="background-image: url('<?php echo get_template_directory_uri(); ?>/image/malioboro.jpg');">
					<div class="container">
					  	<div class="row">
					  		<div class="article">
					  			<div class="bg-img-contact">
					  				<?php 
			                			$args = array(
							                              'post_type'      => 'post',
							                              'category_name'  => 'Contact me 2',
							                              'posts_per_page' => 1
						                              );
			                			$contact_me_2 = new WP_Query($args);
			              			?>
						            <?php if( $contact_me_2 -> have_posts() ) : ?>
						                <?php while ( $contact_me_2 -> have_posts() ) : $contact_me_2 -> the_post(); ?>
					  				<div class="box-contact-desc">
					  					<div class="wrapper-address">
					  						<div class="content-address">					
					  							<h2><?php the_title(); ?></h2>
					  							<br>
					  							<p><?php the_content(); ?></p>
					  							<br>
					  							<a href="https://www.google.com/maps/place/Pracimantoro,+Wonogiri+Regency,+Central+Java/@-8.0785461,110.7438609,12z/data=!3m1!4b1!4m5!3m4!1s0x2e7bcf3662f648a7:0x4027a76e352fef0!8m2!3d-8.0555804!4d110.8083887">
						  							<div class="image-maps">
						  								<?php if(has_post_thumbnail()) : ?>
															<?php the_post_thumbnail(); ?>
															    <?php endif; ?>
						  							</div>					  							
						  								<button>View on maps <span class=" glyphicon glyphicon-play-circle"></span></button>
					  							</a>
					  						</div>
					  					</div>
					  				</div>
					  					<?php endwhile; ?>
									<?php endif; ?>          <!-- end loop contact 2 --> 
					  			</div>
					  		</div>
					  	</div>
					</div>
				</div>
				<div class="wrapper-form-contact">
					<div class="bg-img-contact">
						<div class="container">
							<div class="row">
								<div class="article">
									<div class="form-contact">
										<div class="col-lg-12">
											<div class="entry-data">
													<?php if ( have_posts() ): ?>
            											<?php while ( have_posts() ) : the_post(); ?>
            									<div class="text-center">
													<div class="title-form">
														<h2>Kirim pesan via email disini</h2>
													</div>
												</div>
            													<?php the_content(); ?>
            											<?php endwhile; ?>
            										<?php else :
            													echo 'No Post'; ?>
            										<?php endif; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
</div>

<?php get_footer(); ?>