<?php 

remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );

add_filter('show_admin_bar', '__return_false'); 

function load_file(){
	wp_enqueue_script('deni', get_template_directory_uri() .'/js/deni.js', array('jquery'), null, true);
}

add_action('wp_enqueue_scripts', 'load_file');

function wpb_custom_new_menu() {
  register_nav_menus(
    array(
      'header-menu'         => __( 'Menu Header' ),
      'social-media-menu'   => __( 'Menu Social Media' ),
      'office-address-menu' => __( 'Address Menu' ),
      'useful-menu'         => __( 'Useful Menu' )
    )
  );
}
add_action( 'init', 'wpb_custom_new_menu' );

function get_excerpt_length(){
	return 7;
}


// logo brand 

function themeslug_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 'themeslug_logo_section' , array(
    'title'       => __( 'Logo', 'themeslug' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
) );

    $wp_customize->add_setting( 'themeslug_logo' );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
    'label'    => __( 'Logo', 'themeslug' ),
    'section'  => 'themeslug_logo_section',
    'settings' => 'themeslug_logo',
) ) );
}
add_action( 'customize_register', 'themeslug_theme_customizer' );

function init_setup(){
	add_theme_support('post-thumbnails');
	add_theme_support( 'post-formats', array( 'gallery', 'status' ) );

}

add_action('after_setup_theme', 'init_setup');

// carousel
	
add_action( 'init', 'custom_bootstrap_slider' );
/**
 * Register a Custom post type for.
 */
function custom_bootstrap_slider() {
	$labels = array(
		'name'               => _x( 'Slider', 'post type general name'),
		'singular_name'      => _x( 'Slide', 'post type singular name'),
		'menu_name'          => _x( 'Bootstrap Slider', 'admin menu'),
		'name_admin_bar'     => _x( 'Slide', 'add new on admin bar'),
		'add_new'            => _x( 'Add New', 'Slide'),
		'add_new_item'       => __( 'Name'),
		'new_item'           => __( 'New Slide'),
		'edit_item'          => __( 'Edit Slide'),
		'view_item'          => __( 'View Slide'),
		'all_items'          => __( 'All Slide'),
		'featured_image'     => __( 'Featured Image', 'text_domain' ),
		'search_items'       => __( 'Search Slide'),
		'parent_item_colon'  => __( 'Parent Slide:'),
		'not_found'          => __( 'No Slide found.'),
		'not_found_in_trash' => __( 'No Slide found in Trash.'),
	);

	$args = array(
		'labels'             => $labels,
		'menu_icon'	         => 'dashicons-star-half',
    	'description'        => __( 'Description.'),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array('title','editor','thumbnail')
	);

	register_post_type( 'slider', $args );
}

// widgets

function init_widgets($id){
	register_sidebar(array(
		'name' => 'Sidebar', 
		'id'   => 'sidebar',
		'before_widget' => '<div class="sidebar">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>'
 	));
}

add_action('widgets_init', 'init_widgets');


?>