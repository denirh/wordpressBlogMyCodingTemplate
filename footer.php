<div id="wrapper-footer">
  <div class="bg-img-footer">
    <div class="container footer-section">
      <div class="wrapper-content-footer">
        <div class="wrapper-title">
          <div class="row">
            <div class="col-lg-12">
              <div class="title-footer">
                <h3><?php echo bloginfo('name'); ?></h3>
              </div>
            </div>
          </div>
        </div>
        <div class="wrapper-nav-footer footer-blog-section">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="wrapper-nav">
                <div class="title-nav">
                  <div class="line-nav"></div>
                  <h3>FOLLOW</h3>
                </div>
                <div class="nav-content social-media-nav section-part">
                  <ul class="list-unstyled">
                    <?php $args = array('theme_location'  => 'social-media-menu',
                                        'menu_class'      => 'social-media-menu-class'
                                                ); wp_nav_menu($args); ?>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="wrapper-nav">
                <div class="title-nav">
                  <div class="line-nav"></div>
                  <h3>INFO</h3>
                </div>
                <div class="nav-content section-part">
                  <ul class="list-unstyled">
                    <?php $args = array('theme_location'  => 'office-address-menu',
                                        'menu_class'      => 'office-address-menu-class'
                                                ); wp_nav_menu($args); ?>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="wrapper-nav float-footer-nav">
                <div class="title-nav">
                  <div class="line-nav"></div>
                  <h3>USEFUL</h3>
                </div>
                <div class="nav-content section-part">
                  <ul class="list-unstyled">
                    <?php $args = array('theme_location'  => 'useful-menu',
                                        'menu_class' => 'useful-menu-class'
                                                ); wp_nav_menu($args); ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="wrapper-copyright text-center">
            <div class="copy-title">
              <h5>Copyright by <?php bloginfo('name'); echo ' - ' .date('Y'); ?></h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php wp_footer(); ?>
</body>
</html>