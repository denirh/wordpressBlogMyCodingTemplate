<?php get_header(); ?>

<div class="wrapper-about-page">
	<div class="bg-img-about-page">
		<div class="container about-page-section my-desc">
			<div class="row">
				<div class="article">
					<div class="post">
						<?php 
                			$args = array(
				                              'post_type'      => 'post',
				                              'category_name'  => 'about me 1',
				                              'posts_per_page' => 1
			                              );
                			$about_me_1 = new WP_Query($args);
              			?>
			            <?php if( $about_me_1 -> have_posts() ) : ?>
			                <?php while ( $about_me_1 -> have_posts() ) : $about_me_1 -> the_post(); ?>
						<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
							<div class="wrap-text-me">
								<div class="text-me">
									<div id="line-about"></div>
									<h1><?php the_title(); ?>
									<?php the_content(); ?></h1> 	
								</div>
							</div>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 photo-one">
							<div class="wrap-photo-me">
								<div class="me-photo">
									<?php if(has_post_thumbnail()) : ?>
										<?php the_post_thumbnail(); ?>
										    <?php endif; ?>
								</div>
							</div>
						</div>
							<?php endwhile; ?>
						<?php endif; ?>          <!-- end loop about me 1 --> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="wrapper-description">
	<div class="container about-page-section">
		<div class="row">
			<div class="article">
				<div class="post">
					<?php 
                			$args = array(
				                              'post_type'      => 'post',
				                              'category_name'  => 'about me 2',
				                              'posts_per_page' => 1
			                              );
                			$about_me_2 = new WP_Query($args);
              			?>
			            <?php if( $about_me_2 -> have_posts() ) : ?>
			                <?php while ( $about_me_2 -> have_posts() ) : $about_me_2 -> the_post(); ?>
					<div class="col-lg-12">
						<div class="wrap-description">
							<div class="description-me">
								<h1><?php echo the_title(); ?></h1>
								<br>
								<br>
								<h4><?php echo the_content(); ?></h4>
							</div>
						</div>
					</div>
						<?php endwhile; ?>
					<?php endif; ?>          <!-- end loop about me 2 -->
				</div>
			</div>
		</div>
	</div>
</div>

<div class="wrapper-about-skill">
	<div class="container about-page-section">
		<div class="row">
			<div class="article">
			 	<div class="post">
			 		<?php 
                			$args = array(
				                              'post_type'      => 'post',
				                              'category_name'  => 'about me 3',
				                              'posts_per_page' => 1
			                              );
                			$about_me_3 = new WP_Query($args);
              			?>
			            <?php if( $about_me_3 -> have_posts() ) : ?>
			                <?php while ( $about_me_3 -> have_posts() ) : $about_me_3 -> the_post(); ?>
			 		<div class="col-lg-12">
			 			<div class="wrap-skill-description text-center">
			 				<div class="skill-description ">
			 					<h1><?php the_title(); ?></h1>
			 					<br>
			 					<br>
			 					<p><?php the_content(); ?></p>
			 				</div>
			 			</div>
			 		</div>
			 			<?php endwhile; ?>
					<?php endif; ?>          <!-- end loop about me 3 -->
			 	</div>
			</div>
		</div>
	</div>
</div>

<div class="wrapper-about-end">
	<div class="container about-page-section end-section">
		<div class="row">
			<div class="article">
				<div class="post">
					<?php 
                			$args = array(
				                              'post_type'      => 'post',
				                              'category_name'  => 'about me 4',
				                              'posts_per_page' => 1
			                              );
                			$about_me_4 = new WP_Query($args);
              			?>
			            <?php if( $about_me_4 -> have_posts() ) : ?>
			                <?php while ( $about_me_4 -> have_posts() ) : $about_me_4 -> the_post(); ?>
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
						<div class="wrap-photo-end">
							<div class="photo-end">
								<?php if(has_post_thumbnail()) : ?>
									<?php the_post_thumbnail(); ?>
										<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
						<div class="wrap-desc-end">
							<div class="desc-end">
								<h1><?php the_title(); ?></h1>
								<br>
								<p><?php the_content(); ?></p>
							</div>
						</div>
					</div>
						<?php endwhile; ?>
					<?php endif; ?>          <!-- end loop about me 4 -->
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>