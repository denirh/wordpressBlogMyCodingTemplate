<?php get_header(); ?>

<!-- title -->
<div class="wrapper-title-single">
	<div class="container container-single-post title-single">
		<div class="content">
			<div class="post">
				<div class="title-single">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- content -->
<div class="wrapper-content-single-post">
	<div class="container container-single-post ">
		<div class="all-content-single">
			<div class="row">
				<div class="wrapper-content-single">
					<div class="post-single">
						<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<div class="single-post-content-wrapper">
								<div class="content-single">
									<div class="image-post-single text-center">
										<?php if( has_post_thumbnail() ) : ?>
											<?php the_post_thumbnail(); ?>
												<?php endif; ?>
									</div>
									<div class="desc-post-single">
										<p><?php the_content(); ?></p>
									</div>
								</div>
							</div>
						</div>
							<?php endwhile; ?>
						<?php endif; ?>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="box-sidebar">
								<div class="wrapper-sidebar">
									<div class="author-date">
										<p id="author"><?php echo 'Author by' . ' ' .  get_the_author(); ?> </p>
										<p id="date"><?php echo 'Publish on' . ' ' . date( "F j, Y" ); ?></p>
									</div>
									<div class="sidebar-section">
										<?php if(is_active_sidebar('sidebar')) : ?>
							              	<?php dynamic_sidebar('sidebar'); ?>
							           		 	<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>