<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); echo bloginfo(); ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/style/style.css">
    <?php wp_head(); ?>
  </head>
<body>
<!-- header -->
    <nav class="navbar navbar-default header-top">
        <div class="container padding-header">
            <div class="row">
                <!-- navbar collapse -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <!-- logo brand -->
                        <?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>
                            <div class='site-logo'>
                                <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a> <!-- <span class="desc-logo"><?php echo bloginfo('name'); ?></span> -->
                            </div>
                        <?php else : ?>
                            <hgroup>
                                <h4 class='site-description'><?php bloginfo( 'description' ); ?></h4>
                            </hgroup>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- navbar -->
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav float"> 
                            <?php $args = array('theme_location'  => 'header-menu',
                                                'menu_class'     => 'header-menu-class'
                                                ); wp_nav_menu($args); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>