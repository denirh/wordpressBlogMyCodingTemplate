<?php get_header(); ?>

<div class="wrapper-page-sevice">
		<div class="row">
	      	<div class="col-lg-12">
		      <div class="wrap-page-title">
		        <div class="title-wrapper-blog bg-image-page">
		        	<div class="title-page-section text-center">
		        		<h3><?php the_title(); ?></h3>	
		        	</div>
		        </div>
		      </div>
	        </div>
        </div>
	<div class="container-fluid">
		<div class="wrapper-content">
			<div class="row">
				<div class="article">
					<div class="post">
						<?php 
                			$args = array(
				                              'post_type'      => 'post',
				                              'category_name'  => 'wp dev service',
				                              'posts_per_page' => 1
			                              );
                			$page_wp_dev = new WP_Query($args);
              			?>
			            <?php if( $page_wp_dev -> have_posts() ) : ?>
			                <?php while ( $page_wp_dev -> have_posts() ) : $page_wp_dev -> the_post(); ?>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
											<div class="content-post-service text-center">
												<div class="post-image-and-text">
													<div class="wrap animation-service">
														<div class="image-post-service-page">
															<?php if(has_post_thumbnail()) : ?>
										                      <?php the_post_thumbnail(); ?>
										                        <?php endif; ?>
														</div>													
														<h3><?php the_title(); ?></h3>
															<div class="line-service"></div>											
														<p><?php the_content(); ?></p>
													</div>
												</div>
											</div>
										</div>
							<?php endwhile; ?>
						<?php endif; ?>
														<!-- info grafik -->
						<?php 
                			$args = array(
				                              'post_type'      => 'post',
				                              'category_name'  => 'info grafik',
				                              'posts_per_page' => 1
			                              );
                			$page_wp_dev2 = new WP_Query($args);
              			?>
			            <?php if( $page_wp_dev2 -> have_posts() ) : ?>
			                <?php while ( $page_wp_dev2 -> have_posts() ) : $page_wp_dev2 -> the_post(); ?>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 info-grafik ">
											<div class="content-post-service text-center">
												<div class="post-image-and-text">
													<div class="wrap animation-service">
														<div class="image-post-service-page">
															<?php if(has_post_thumbnail()) : ?>
										                      <?php the_post_thumbnail(); ?>
										                        <?php endif; ?>
														</div>													
														<h3><?php the_title(); ?></h3>
															<div class="line-service"></div>											
														<p><?php the_content(); ?></p>
													</div>
												</div>
											</div>
										</div>
							<?php endwhile; ?>
						<?php endif; ?>         <!-- end info grafik -->
												<!-- designing -->
						<?php 
                			$args = array(
				                              'post_type'      => 'post',
				                              'category_name'  => 'designing',
				                              'posts_per_page' => 1
			                              );
                			$page_wp_dev3 = new WP_Query($args);
              			?>
			            <?php if( $page_wp_dev3 -> have_posts() ) : ?>
			                <?php while ( $page_wp_dev3 -> have_posts() ) : $page_wp_dev3 -> the_post(); ?>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
											<div class="content-post-service text-center">
												<div class="post-image-and-text">
													<div class="wrap animation-service">
														<div class="image-post-service-page">
															<?php if(has_post_thumbnail()) : ?>
										                      <?php the_post_thumbnail(); ?>
										                        <?php endif; ?>
														</div>													
														<h3><?php the_title(); ?></h3>
															<div class="line-service"></div>											
														<p><?php the_content(); ?></p>
													</div>
												</div>
											</div>
										</div>
							<?php endwhile; ?>
						<?php endif; ?>      <!-- end designing -->
					</div>
				</div>							
			</div>
		</div>							
	</div>
</div>

<?php get_footer(); ?>