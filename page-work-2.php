<?php
/*
Template Name: Project
*/
?>

<?php get_header(); ?>

<!-- project -->
<div class="wrapper-page">
    <div class="row">
        <div class="col-lg-12">
        <div class="wrap-page-title">
          <div class="title-wrapper-blog bg-image-page">
            <div class="title-page-section text-center">
              <h3><?php the_title(); ?></h3> 
            </div>
          </div>
        </div>
      </div>
    </div>
  <div class="container page-project-section">
    <div class="wrapper-all-content-project">      
        <div class="wrapper-content-project"> 
          <div class="row">
            <div class="article" >
              <div class="post" >
                <?php 
                      $args = array(
                                       'post_type'     => 'post',
                                       'category_name' => 'project',
                                       'posts_per_page' => 6
                                    );
                      $custom_query = new WP_Query( $args );

                ?>
                    <?php if ( $custom_query->have_posts() ) : ?>
                    <!-- the loop -->
                      <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
                      <div class="col-sm-6 col-md-4 work-page-post">
                        <div class="thumbnail">
                          <?php if(has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail(); ?>
                              <?php endif; ?>
                            <div class="caption caption-blog-page">
                              <h3 style="margin-top: 15px;"><?php the_title()?></h3>
                                <div class="view-more-blog-page">
                                  <a href="<?php the_permalink(); ?>" class="btn btn-default btn-xs btn-view-more" role="button">View More</a>
                                </div>
                            </div>
                        </div>
                      </div>
                      <?php endwhile;?>
                    <?php endif;?>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<?php get_footer(); ?> 