<!DOCTYPE html>
<html>
<head>
	<title>Eror 404</title>
	   <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet"> 
<style>

	#emp {
		font-size: 100px; 
		margin: 0px; 
		color: #FFC300;
		font-family: "roboto slab";
	}

	#halm {
		font-size: 50px; 
		margin: -30px 0px 30px 0px;
		color: #FFC300;
		font-family: "roboto slab";
	}

	body {
		width: 100%;
	}
	div {
		 padding-top: 150px;
		text-align: center;
	}
	a {
		text-decoration: none; 
		color: #3f3736; 
		font-size: 20px;
		font-family:'roboto slab';
	}
	a:hover {
		color: #FFC300;
	}

	span {
		padding: 15px 15px;
		border-radius: 5px;
		background: white;
	}

	@media only screen and (max-width: 480px){

		div {
			padding-top: 170px;
		}
		
		#emp {
			font-size: 50px;
		}
		#halm {
			font-size: 30px; 
			margin: 0px 0px 30px 0px;
			color: #FFC300;
			font-family: "roboto slab";
		}

		span	{
			padding: 10px 10px;
			border-radius: 5px;
			background: white;
		}

	}
</style>
</head>
	<body style="background-color: #3f3736;" >
			<div >
				<p id="emp">Eror 404</p>
					<p id="halm"> Halaman tidak ditemukan </p>
				<a href="<?php echo home_url();?>"><span>Home</span></a>
			</div>
	</body>
</html>