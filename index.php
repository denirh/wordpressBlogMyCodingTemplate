<?php get_header(); ?>

<!-- carousel -->
<div id="img-slide" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#img-slide" data-slide-to="0" class="active"></li>
    <li data-target="#img-slide" data-slide-to="1"></li>
    <li data-target="#img-slide" data-slide-to="2"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php $slider = get_posts(array('post_type' => 'slider', 'posts_per_page' => 3)); ?>
      <?php $count = 0; ?>
        <?php foreach($slider as $slide): ?>
    <div class="item <?php echo ($count == 0) ? 'active' : ''; ?>">
      <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($slide->ID)) ?>" class="img-responsive"/>
        <div class="carousel-caption">
          <div class="full-width text center desc-carousel">
          	
          </div>
        </div>
    </div>
      <?php $count++; ?>
        <?php endforeach; ?>
  </div>
  <!-- Controls -->
  <a class="left carousel-control" href="#img-slide" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#img-slide" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<!-- hoby -->
<div id="service-session">
  <section class="content-web bg-img">
    <div class="container service-section">
      <div class="wrapper-all-content-service">
        <div class="row"> 
          <div class="col-lg-12">
            <div class="title we-can-help-you">
              <div class="line"></div>
                <h1>My Hoby</h1>
            </div>
          </div>
        </div>
        <div class="row row-table">
          <div class="content-wrap">
            <?php 
                    $args = array(
                                     'post_type'     => 'post',
                                     'category_name' => 'Post skill',
                                     'posts_per_page' => 4
                                  );
                    $post_skill = new WP_Query( $args );

              ?>
                  <?php if ( $post_skill->have_posts() ) : ?>
                  <!-- the loop -->
                    <?php while ( $post_skill->have_posts() ) : $post_skill->the_post(); ?>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 content-image-text">
                <div class="col-content service-home">
                  <figure>
                    <?php if(has_post_thumbnail()) : ?>
                      <?php the_post_thumbnail(); ?>
                        <?php endif; ?>
                  </figure>
                  <article>
                    <p><?php the_title()?></p>
                  </article>
                </div>
            </div>
              <?php endwhile;?>
            <?php endif;?>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- project -->
<div class="container project-section work-home">
  <div class="wrapper-all-content-project">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="title projects-desc">
          <div class="line-project"></div>
            <h1 style="margin-top: 10px; color: #989898;">Projects</h1>
        </div> 
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="view-all-work">
          <a class="btn btn-primary" href="work-2">View All Projects</a>
        </div>
      </div>
    </div>
      <div class="wrapper-content-project"> 
        <div class="row">
          <div class="article" >
            <div class="post" >
              <?php 
                    $args = array(
                                     'post_type'     => 'post',
                                     'category_name' => 'project',
                                     'posts_per_page' => 6
                                  );
                    $custom_query = new WP_Query( $args );

              ?>
                  <?php if ( $custom_query->have_posts() ) : ?>
                  <!-- the loop -->
                    <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
                    <div class="col-sm-6 col-md-4">
                      <div class="thumbnail project-front work-home-page-parallax">
                        <?php if(has_post_thumbnail()) : ?>
                          <?php the_post_thumbnail(); ?>
                            <?php endif; ?>
                        <div class="caption project-home">
                          <h3 style="margin-top: 15px;"><?php the_title()?></h3>
                            <p><a href="<?php the_permalink(); ?>" class="btn btn-default btn-xs" role="button">View More</a></p>
                        </div>
                      </div>
                    </div>
                    <?php endwhile;?>
                  <?php endif;?>
            </div>
          </div>
        </div>
      </div>
        <div class="wrapper-button-hidden">
          <div class="row">
            <div class="col-xs-12">
              <div class="view-all-work-hidden">
                <a class="btn btn-info" href="work-2">View All Projects</a>
              </div>
            </div>
          </div>
        </div>
  </div>
</div>

<!-- blog -->
<div class="wrapper-blog-home blog-home-section">
  <div class="container blog-section">
    <div class="content-blog-home">
      <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
          <div class="title from-our-blog">
            <div class="line-blog"></div>
              <h1>From our blogs</h1>
          </div> 
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="view-all-work">
            <a class="btn btn-primary btn-index-blog" href="blog-2">View All Blog</a>
          </div>
        </div>
      </div>
        <div class="wrap">
          <div class="row">
            <div class="article">
              <div class="post">
              <?php 
                $args = array(
                              'post_type'      => 'post',
                              'category_name'  => 'blog',
                              'posts_per_page' => 4 
                              );
                $custom_query2 = new WP_Query($args);
              ?>
              <?php if( $custom_query2 -> have_posts() ) : ?>
                <?php while ( $custom_query2 -> have_posts() ) : $custom_query2 -> the_post(); ?>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="thumbnail blog-home hidden-mobile blog-home-index front-blog">
                    <a href="<?php the_permalink(); ?>">
                    <?php if(has_post_thumbnail()) : ?>
                      <?php the_post_thumbnail(); ?>
                        <?php endif; ?>
                      <div class="caption caption-blog">
                        <h4 style="margin-top: 15px;"><?php the_title(); ?></h4>
                      </div>
                    </a>
                  </div>
                </div>
                <?php endwhile; ?>
              <?php endif; ?>
              </div> <!-- end post -->
            </div> <!-- end article -->
          </div> <!-- end row -->
        </div> <!-- end wrap -->
    <div class="wrapper-button-hidden">
      <div class="row">
        <div class="col-xs-12">
          <div class="view-all-work-hidden">
            <a class="btn btn-info" href="blog-2">View All Blog</a>
          </div>
        </div>
      </div>
    </div> <!-- end content blog home -->
  </div> <!-- end container -->
</div> <!-- end -->

<?php get_footer(); ?>